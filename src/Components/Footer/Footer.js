import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Repponsive";
import FooterDesktop from "./FooterDesktop";
import FooterMobi from "./FooterMobi";
import FooterTablet from "./FooterTablet";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>
      <Tablet>
        {" "}
        <FooterTablet />
      </Tablet>
      <Mobile>
        {" "}
        <FooterMobi />
      </Mobile>
    </div>
  );
}
