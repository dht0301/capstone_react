import React from "react";
import logo_payoo from "../../assets/logo-payoo.png";
import logo_zalopay from "../../assets/ZaloPay_logo.webp";
import logo_bidv from "../../assets/Logo-BIDV-Ori.webp";
import logo_momo from "../../assets/MoMo_Logo.png";
import logo_ios from "../../assets/download.png";
import logo_android from "../../assets/download (1).png";
import logo_facebook from "../../assets/download (2).png";
import logo_zalo from "../../assets/download (3).png";
import logo_bocongthuong from "../../assets/logo-da-thong-bao-voi-bo-cong-thuong.png";

export default function FooterPage() {
  return (
    <div className="bg-[#A6BB8D] main-footer text-black">
      <div className="container mx-auto">
        <div className="grid grid-cols-4 gap-4 p-2 ">
          <div>
            <h4 className="font-bold mb-4">TIX</h4>
            <div className="flex">
              <ul className="list-unstyle grid grid-rows-3 grid-flow-col gap-4">
                <li className="">
                  <a href="">FAQ</a>
                </li>
                <li>
                  <a href="">Thỏa thuận sử dụng</a>
                </li>

                <li>
                  <a href="">Chính sách bảo mật</a>
                </li>
              </ul>
            </div>
            <div>
              <img className="w-44 h-20" src={logo_bocongthuong} alt="" />
            </div>
          </div>
          <div className="">
            <h4 className="font-bold mb-4 ml-5">ĐỐI TÁC</h4>
            <div className="flex">
              <div className="mx-auto">
                <a href="">
                  <img
                    className="w-10 h-10 max-w-lg mx-auto"
                    src={logo_zalopay}
                    alt=""
                  />
                </a>
              </div>
              <div className="mx-auto">
                <a href="">
                  <img
                    className="w-10 h-10 max-w-lg mx-auto"
                    src={logo_payoo}
                    alt=""
                  />
                </a>
              </div>
              <div className="mx-auto">
                <a href="">
                  <img
                    className="w-10 h-10 max-w-lg mx-auto"
                    src={logo_momo}
                    alt=""
                  />
                </a>
              </div>
              <div className="mx-auto">
                <a href="">
                  <img
                    className="w-10 h-10 max-w-lg mx-auto"
                    src={logo_bidv}
                    alt=""
                  />
                </a>
              </div>
            </div>
          </div>
          <div className="font-bold mb-4">
            <h4 className="font-bold mb-4">MOBILE APP</h4>
            <div className="flex mx-auto">
              <div className="mr-2 grid-cols-2">
                <a href="">
                  <img className="w-10 h-10  mx-auto" src={logo_ios} alt="" />
                </a>
              </div>
              <div className=" grid-cols-2">
                <a href="">
                  <img
                    className="w-10 h-10 mx-auto"
                    src={logo_android}
                    alt=""
                  />
                </a>
              </div>
            </div>
          </div>
          <div className="font-bold mb-4">
            <h4 className="font-bold mb-4 ">SOCIAL</h4>
            <div className="flex mx-auto">
              <div className="mr-2 grid-cols-2">
                <a href="">
                  <img
                    className="w-10 h-10  mx-auto"
                    src={logo_facebook}
                    alt=""
                  />
                </a>
              </div>
              <div className="mr-2 grid-cols-2">
                <a href="">
                  <img className="w-10 h-10  mx-auto" src={logo_zalo} alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr className=" mx-10 h-px my-8 bg-gray-600 border-0 dark:bg-gray-70" />
      <div className="container mx-auto">
        <div className="mt-5">
          <p className="text-center text-black">© 2023 Cinema HD Cineplex</p>
        </div>
      </div>
    </div>
  );
}
