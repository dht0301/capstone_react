import { Table } from "antd";
import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Repponsive";
import HeaderDestop from "./HeaderDestop";
import HeaderMobile from "./HeaderMobile";
import HeaderTaplet from "./HeaderTaplet";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDestop />
      </Desktop>
      <Tablet>
        <HeaderTaplet />
      </Tablet>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}
