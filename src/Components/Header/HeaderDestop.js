import React from "react";
import { NavLink } from "react-router-dom";
import UseNav from "./UseNav";

export default function HeaderDestop() {
  return (
    <div className="px-20  py-10 flex justify-between items-center shadow-lg ">
      <NavLink to={"/"}>
        <span className="text-red-600 text-3xl font-medium"> CyberFlix</span>
      </NavLink>
      <ul className="flex">
        <li className="nav-item active">
          <NavLink className="nav-link" to="/">
            Phim
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/">
            Cụm Rạp
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/">
            Tin Tức
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/">
            Ứng dụng
          </NavLink>
        </li>
      </ul>

      <UseNav />
    </div>
  );
}
