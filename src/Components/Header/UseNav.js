import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../Pages/Service/localService";

export default function UseNav() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const handleLogout = () => {
    userLocalService.remove();
    // window.location.href("/login");
    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <div>
            <span>{user?.hoTen}</span>
            <button
              onClick={handleLogout}
              className="border-2 border-black rounded px-5 py-2 "
            >
              Đăng Xuất
            </button>
          </div>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-black rounded px-5 py-2 "
          >
            Đăng nhập
          </button>
          <NavLink to="/register">
            <button className="border-2 border-black rounded px-5 py-2 ">
              Đăng ký
            </button>
          </NavLink>
        </>
      );
    }
  };
  return <div className="space-x-3"> {renderContent()}</div>;
}
