import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import Detail from "./Pages/Detail/Detail";
import Layout from "./HOC/Layout";
import AdminUserPage from "./Pages/AdminUser/AdminUserPage";
import AdminMoviePage from "./Pages/AdminMoviePage/AdminMoviePage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={
              <Layout>
                <Detail />
              </Layout>
            }
          />
          <Route path="/admin/movie" element={<AdminMoviePage />} />
          <Route path="/admin/user" element={<AdminUserPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
