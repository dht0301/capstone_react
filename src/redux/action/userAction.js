import { message } from "antd";
import { postLogin } from "../../Pages/Service/userService";
import { SET_USER_LOGIN } from "../constans/userConstan";

export const setUserAction = (value) => {
  return { type: SET_USER_LOGIN, payload: value };
};

export const setUserActionService = (value, onSuccess) => {
  return (dispatch) => {
    postLogin(value)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch({ type: SET_USER_LOGIN, payload: res.data.content });
        onSuccess();
      })
      .catch((err) => {
        message.success("Đăng nhập thành công");
      });
  };
};
