import React, { useEffect, useState } from "react";
import { deleteUserList, getUserList } from "../Service/adminService";
import { Button, message, Space, Table, Tag } from "antd";
import { userColums } from "./util";
import Build from "lottie-react";

export default function AdminUserPage() {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let handleRemove = (accout) => {
      deleteUserList(accout)
        .then((res) => {
          console.log("res: ", res);
          message.success("xóa thành công");
          fetUserList();
        })
        .catch((err) => {
          console.log("err: ", err);
          message.error(err.responese.data.content);
        });
    };
    let fetUserList = () => {
      getUserList()
        .then((res) => {
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              key: item.taiKhoan,
              action: (
                <div className=" space-x-3">
                  <Button
                    onClick={() => {
                      handleRemove(item.taiKhoan);
                    }}
                    type="primary"
                    danger
                  >
                    xóa
                  </Button>
                  <Button type="dashed">sửa</Button>
                </div>
              ),
            };
          });
          setUserArr(userList);
        })

        .catch((err) => {});
    };
    fetUserList();
  }, []);

  return (
    <div>
      <Table columns={userColums} dataSource={userArr} />
    </div>
  );
}
