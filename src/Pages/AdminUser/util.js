import { Tag } from "antd";

export const userColums = [
  {
    title: "tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Tài Khoản ",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Email ",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Loại người dùng ",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      if (text == "QuanTri") {
        return <Tag color="red">Quản Trị</Tag>;
      } else {
        return <Tag color="red"> khách hàng</Tag>;
      }
    },
  },
  { title: "hành động", dataIndex: "action", key: "action" },
];
