import moment from "moment/moment";
import React from "react";
import "moment/locale/vi";
moment.locale("vi");
export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-10 space-x-5">
      <img className="w-28 h-40 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="font-medium text-2xl">{movie.tenPhim}</h3>
        <div className="grid grid-cols-3 gap-7 mt-2">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu) => {
            return (
              <p className="bg-red-500 text-white px-3 py-2 rounded font-medium">
                {moment(lichChieu.ngayChieuGioChieu).format("DD/MM hh:mm a")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}
