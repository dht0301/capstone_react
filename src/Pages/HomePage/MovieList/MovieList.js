import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function MovieList({ movieArr }) {
  const rederMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
      return (
        <Card
          hoverable
          style={{
            width: 240,
          }}
          cover={
            <img
              alt="example"
              className="h-60 object-cover"
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            title={<h2 className="text-blue-500 h-10">{item.tenPhim}</h2>}
            className=""
          />
          <NavLink
            className="bg-red-600 text-white px-5 py-2 rounded"
            to={`/detail/${item.maPhim}`}
          >
            Xem chi tiết
          </NavLink>
        </Card>
      );
    });
  };
  return (
    <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 mx-auto gap-4">
      {rederMovieList()}
    </div>
  );
}
